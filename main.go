package main

import (
	"bufio"
	"context"
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"os"
	"strings"
	"time"
)

var database *sql.DB

const bdd = "root" + ":" + "example" + "@tcp(" + "127.0.0.1" + ":" + "3307" + ")/" + "gtfs"

const filename string = "/Users/azorsteven/Downloads/stop_times.txt"

const query = "INSERT INTO `stop_time`(`trip_id`,`arrival_time`,`departure_time`,`stop_id`,`stop_sequence`,`pickup_type`,`drop_off_type`) VALUES(?,?,?,?,?,?,?)"

var lines chan string
var count = 0

var lastcount = 0

func main() {
	lines = make(chan string, 100000)
	createConnection()
	go metric()
	go IntegrateLine()

	readFileLineByLine()
}

func createConnection() {
	var err error
	database, err = sql.Open("mysql", bdd)
	if err != nil {
		fmt.Println(err.Error())
	}
}

func readFileLineByLine() {

	file, err := os.Open(filename)
	if err != nil {
		fmt.Println(err.Error())
	}

	fileScanner := bufio.NewScanner(file)
	fileScanner.Split(bufio.ScanLines)

	for fileScanner.Scan() {
		lines <- fileScanner.Text()
	}
}

func IntegrateLine() {
	for line := range lines {
		insert(parseLine(line))
	}
}

func insert(values []string) {

	_, err := database.ExecContext(context.Background(), query, values[0], values[1], values[2], values[3], values[4], values[5], values[6])
	if err != nil {
		fmt.Println(err.Error())
	} else {
		count++
	}

}

func parseLine(line string) []string {
	table := strings.Split(line, ",")

	return table
}

func metric() {
	for {
		var diff = count - lastcount
		lastcount = count
		fmt.Println(diff, " insertions ")
		time.Sleep(1 * time.Second)
	}
}
